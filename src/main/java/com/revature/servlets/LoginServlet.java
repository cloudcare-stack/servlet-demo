package com.revature.servlets;

import com.revature.daos.EmployeeDao;
import com.revature.daos.EmployeeDaoImpl;
import com.revature.models.Employee;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;

public class LoginServlet extends HttpServlet {

    private EmployeeDao employeeDao = new EmployeeDaoImpl();

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        PrintWriter out = response.getWriter();
        response.setContentType("text/html");
        String username = request.getParameter("userName");
        String password = request.getParameter("passWord");
        //String managerParams = request.getParameter("is-manager"); ???

        Employee employee = employeeDao.getEmployeeByUsernameAndPassword(username, password);

        try {
            String managerParams = employeeDao.authenticateUser(employee);

            if (managerParams.equals("true")) {
                out.println("<Center><h3>");
                out.println("Forwarding Request.........");
                out.println("</h3></Center>");
                // forwarding using Request Dispatcher
//            ServletContext servletContext = getServletContext();
//            RequestDispatcher requestDispatcher = servletContext
//                    .getRequestDispatcher("/reimbursements");
//            requestDispatcher.forward(request, response);
                response.setStatus(201);
                response.setHeader("Content-type", "text/html");
                response.sendRedirect(request.getContextPath() + "/views/manager/all-reimbursements.html");

            } else if (managerParams.equals("false")) {
                response.setStatus(201);
                response.setHeader("Content-type", "text/html");
                response.sendRedirect(request.getContextPath() + "/views/employee/my-reimbursements.html");

            } else {
                ServletContext servletContext = getServletContext();
                RequestDispatcher requestDispatcher = servletContext
                        .getRequestDispatcher("/index.html");
                requestDispatcher.forward(request, response);
            }
        } catch (IOException e) {
            e.printStackTrace();
        } catch (ServletException e) {
            e.printStackTrace();
        }


    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException{
        doPost(request, response);
    }
}
