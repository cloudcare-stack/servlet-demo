package com.revature.servlets;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.revature.daos.EmployeeDao;
import com.revature.daos.EmployeeDaoImpl;
import com.revature.daos.ReimbursementDao;
import com.revature.daos.ReimbursementDaoImpl;
import com.revature.models.Employee;
import com.revature.models.Reimbursement;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;

public class EmployeeServlet extends HttpServlet {

    private ReimbursementDao reimbursementDao = new ReimbursementDaoImpl();
    private EmployeeDao employeeDao = new EmployeeDaoImpl();
    private ObjectMapper om = new ObjectMapper();
    private ObjectMapper objectMapper = new ObjectMapper();

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        //Successfully populate pop-down selection
        String employeeJSON = om.writeValueAsString(employeeDao.getAllEmployees());
        response.setHeader("Content-Type", "application/json");
        response.setCharacterEncoding("UTF-8");
        try(PrintWriter pw = response.getWriter()){
            pw.write(employeeJSON);
        }

//        String employeeId = request.getPathInfo();
//        String[] employee = employeeId.split("/");
//        int partOne = Integer.parseInt(employee[1]);
//        System.out.println(partOne);
//
//        System.out.println(partOne);
//        if(partOne == reimbursementDao.getAllReimbursements().get(0).getId()){
//            String reimburseJSON = om.writeValueAsString(reimbursementDao.getReimbursementsByEmployee(partOne));
//            response.setHeader("Content-Type", "application/json");
//            response.setCharacterEncoding("UTF-8");
//            try(PrintWriter pw = response.getWriter()){
//                pw.write(reimburseJSON);
//            }
//        }

        //Not able to print reimbursements by employee ???
//        String reimburseJSON = om.writeValueAsString(reimbursementDao.getReimbursementsByEmployee(1));
//        response.setHeader("Content-Type", "application/json");
//        response.setCharacterEncoding("UTF-8");
//        try(PrintWriter pw = response.getWriter()){
//            pw.write(reimburseJSON);
//        }


//        String employeeId = request.getPathInfo();
//        String[] employee = employeeId.split("/");
//        String partOne = employee[1];
//        System.out.println(partOne);


//        HttpSession session = request.getSession(false);
//        String n = (String)session.getAttribute("employeeId");

//        String reimburseJSON = objectMapper.writeValueAsString(reimbursementDao.getReimbursementsByEmployee(Integer.parseInt(partOne)));
//        try(PrintWriter printWriter = response.getWriter()){
//            printWriter.write(n);
//        }

//        System.out.println(query);

        //System.out.println(query.contains("false"));

//        if(query.contains("false")){
//            managerParams = "false";
//            response.sendRedirect(getReqURL + managerParams);
//        }
//        else{
//            managerParams = "true";
//            response.sendRedirect(getReqURL + managerParams);
//
//        }





    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException{
        doGet(request, response);
    }

}
