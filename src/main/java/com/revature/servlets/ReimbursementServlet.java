package com.revature.servlets;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.revature.daos.ReimbursementDao;
import com.revature.daos.ReimbursementDaoImpl;
import com.revature.models.Reimbursement;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;
import java.util.Locale;

public class ReimbursementServlet extends HttpServlet {

    private ReimbursementDao reimbursementDao = new ReimbursementDaoImpl();
    private ObjectMapper om = new ObjectMapper();

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        String statusParams = request.getParameter("status");

//        HttpSession session = request.getSession();
//        session.setAttribute("employeeId", reimbursementJSON);

        if (statusParams == null) {
            String reimbursementJSON = om.writeValueAsString(reimbursementDao.getAllReimbursements());
            response.setHeader("Content-type", "application/json");
            response.setCharacterEncoding("UTF-8");
            try (PrintWriter pw = response.getWriter()) {
                pw.write(reimbursementJSON);
            }
        } else if (statusParams.equals("APPROVED")) {
            String approvedJSON = om.writeValueAsString(reimbursementDao.getApprovedReimbursements());
            response.setHeader("Content-type", "application/json");
            response.setCharacterEncoding("UTF-8");
            try (PrintWriter pw = response.getWriter()) {
                pw.write(approvedJSON);
            }
        } else if (statusParams.equals("DENIED")) {
            String deniedJSON = om.writeValueAsString(reimbursementDao.getDeniedReimbursements());
            response.setHeader("Content-type", "application/json");
            response.setCharacterEncoding("UTF-8");
            try (PrintWriter pw = response.getWriter()) {
                pw.write(deniedJSON);
            }
        } else if (statusParams.equals("PENDING")) {
            String pendingJSON = om.writeValueAsString(reimbursementDao.getPendingReimbursements());
            response.setHeader("Content-type", "application/json");
            response.setCharacterEncoding("UTF-8");
            try (PrintWriter pw = response.getWriter()) {
                pw.write(pendingJSON);
            }
        }
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException{
//        doGet(request, response);
    }

}
