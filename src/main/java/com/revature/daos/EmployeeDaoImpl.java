package com.revature.daos;

import com.revature.models.Employee;
import com.revature.util.ConnectionUtil;

import java.sql.*;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

public class EmployeeDaoImpl implements EmployeeDao {

    @Override
    public Employee getEmployeeByUsernameAndPassword(String username, String password){

        Employee employee = null;
        //Establish a connection with try-catch block
        try(Connection conn = ConnectionUtil.getConnection()) {
            String sql = "select * from employee where email = ? and pass = ?";
            PreparedStatement ps = conn.prepareStatement(sql);

            ps.setString(1, username);
            ps.setString(2, password);

            ResultSet rs = ps.executeQuery();

            //Employee employee = null;

            while(rs.next()){
                //iterate each row of data, extracting the relevant fields
                int id = rs.getInt("employee_id");
                String firstName = rs.getString("first_name");
                String lastName = rs.getString("last_name");
                LocalDate birthday = rs.getObject("birthday", LocalDate.class);
                String userName = rs.getString("email");
                String passWord = rs.getString("pass");
                boolean status = rs.getBoolean("status");

                //Populate an employee object
                employee = new Employee(id, firstName, lastName, birthday, userName, passWord, status);
            }



        } catch (SQLException ex) {
            ex.printStackTrace();
        }
        return employee;

    }

    @Override
    public List<Employee> getAllEmployees(){

        // 1. Establish a connection with try/catch block
        // 2. create a statement to obtain a table with try/catch block
        try(Connection conn = ConnectionUtil.getConnection(); Statement s = conn.createStatement();) {

            // 3. Get a result from DB
            ResultSet rs = s.executeQuery("select * from employee");
            // 4. Create Arraylist object to add each row in a list
            List<Employee> employees = new ArrayList<>();

            // 5. While ResultSet object points to a current row of data, next method moves to next row until no more
            while(rs.next()){
                // 6. Iterate each row to obtain values from each column
                int id = rs.getInt("employee_id");
                String firstName = rs.getString("first_name");
                String lastName = rs.getString("last_name");
                LocalDate birthday = rs.getObject("birthday", LocalDate.class);
                String email = rs.getString("email");
                String pass = rs.getString("pass");
                boolean status = rs.getBoolean("status");

                // 7. Populate all values into Employee object
                Employee e = new Employee(id, firstName, lastName, birthday, email, pass, status);
                // 8. Implement Employee Object into ArrayList
                employees.add(e);
            }
            return employees;

        } catch (SQLException e) {
            e.printStackTrace();
        }


        return null;

    }

    @Override
    public List<Employee> getGeneralEmployees(){
// 1. Establish a connection with try/catch block
        // 2. create a statement to obtain a table with try/catch block
        try(Connection conn = ConnectionUtil.getConnection(); Statement s = conn.createStatement();) {

            // 3. Get a result from DB
            ResultSet rs = s.executeQuery("select * from employee");
            // 4. Create Arraylist object to add each row in a list
            List<Employee> employees = new ArrayList<>();

            // 5. While ResultSet object points to a current row of data, next method moves to next row until no more
            while(rs.next()){
                // 6. Iterate each row to obtain values from each column
                int id = rs.getInt("employee_id");
                String firstName = rs.getString("first_name");
                String lastName = rs.getString("last_name");
                LocalDate birthday = rs.getObject("birthday", LocalDate.class);
                String email = rs.getString("email");
                String pass = rs.getString("pass");
                boolean status = rs.getBoolean("status");

                // 7. Populate all values into Employee object
                Employee e = new Employee(id, firstName, lastName, birthday, email, pass, status);
                if(e.isStatus() == true){
                    continue;
                }
                // 8. Implement Employee Object into ArrayList
                employees.add(e);
            }

            return employees;

        } catch (SQLException e) {
            e.printStackTrace();
        }

        return null;

    }

    @Override
    public List<Employee> getManagerEmployees(){
        // 1. Establish a connection with try/catch block
        // 2. create a statement to obtain a table with try/catch block
        try(Connection conn = ConnectionUtil.getConnection(); Statement s = conn.createStatement();) {

            // 3. Get a result from DB
            ResultSet rs = s.executeQuery("select * from employee");
            // 4. Create Arraylist object to add each row in a list
            List<Employee> employees = new ArrayList<>();

            // 5. While ResultSet object points to a current row of data, next method moves to next row until no more
            while(rs.next()){
                // 6. Iterate each row to obtain values from each column
                int id = rs.getInt("employee_id");
                String firstName = rs.getString("first_name");
                String lastName = rs.getString("last_name");
                LocalDate birthday = rs.getObject("birthday", LocalDate.class);
                String email = rs.getString("email");
                String pass = rs.getString("pass");
                boolean status = rs.getBoolean("status");

                // 7. Populate all values into Employee object
                Employee e = new Employee(id, firstName, lastName, birthday, email, pass, status);
                if(e.isStatus()){
                    // 8. Implement Employee Object into ArrayList
                    employees.add(e);
                }
            }

            return employees;

        } catch (SQLException e) {
            e.printStackTrace();
        }

        return null;

    }

    @Override
    public String authenticateUser(Employee employee){

        // validate user input
        if(employee.getEmail()==null || employee.getEmail().isEmpty()){
            return "Invalid user credentials";
        }

        String userName = employee.getEmail();
        String passWord = employee.getPassword();

        System.out.println("Email: " + userName);
        System.out.println("Password: " + passWord);

        String userNameDB = "";
        String passWordDB = "";
        boolean roleDB;

        try(Connection connection = ConnectionUtil.getConnection()){
            Statement statement = connection.createStatement();
            ResultSet resultSet = statement.executeQuery("select email, pass, status from employee");
            while(resultSet.next()){
                userNameDB = resultSet.getString("email");
                passWordDB = resultSet.getString("pass");
                roleDB = resultSet.getBoolean("status");

                if(userName.equals(userNameDB) && passWord.equals(passWordDB) && roleDB == true){
                    return "true";
                } else if(userName.equals(userNameDB) && passWord.equals(passWordDB) && roleDB == false){
                    return "false";
                }

            }


        } catch (SQLException e) {
            e.printStackTrace();
        }

        return "Invalid user credentials";
    }


}
