package com.revature.daos;

import com.revature.models.Employee;
import com.revature.models.ReimburseStatus;
import com.revature.models.Reimbursement;
import com.revature.util.ConnectionUtil;

import java.math.BigDecimal;
import java.sql.*;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

public class ReimbursementDaoImpl implements ReimbursementDao {
    @Override
    public List<Reimbursement> getAllReimbursements() {
        // 1. Establish a connection with try/catch block
        try(Connection conn = ConnectionUtil.getConnection(); Statement s = conn.createStatement();){
            // 3. Get a result from DB
            ResultSet rs = s.executeQuery("select * from reimbursement");
            // 4. Create Arraylist object to add each row in a list
            List<Reimbursement> reimbursements = new ArrayList<>();

            while(rs.next()){
                int id = rs.getInt("reimburse_id");
                String description = rs.getString("description");
                double total = rs.getDouble("total");
                LocalDate date = rs.getObject("purchase_date", LocalDate.class);

                // Access status from the db
                int statusOrdinal = rs.getInt("status");
                // Then use that number to determine the corresponding enum value
                ReimburseStatus[] statuses = ReimburseStatus.values();
                ReimburseStatus status = statuses[statusOrdinal];

                int employee_id = rs.getInt("employee_id");

                Reimbursement r = new Reimbursement(id, description, total, date, status, employee_id);
                reimbursements.add(r);
            }
            System.out.println(reimbursements);
            return reimbursements;

        } catch (SQLException e) {
            e.printStackTrace();
        }


        return null;

    }

    @Override
    public boolean createReimbursement(Reimbursement reimbursement) {
        //validate input to make sure it's not null
        if(reimbursement == null){
            return false;
        }
        //validate input to make sure it has all of the important fields
        if((reimbursement.getDescription() == null) || (reimbursement.getDate() == null)){
            return false;
        }

        try(Connection conn = ConnectionUtil.getConnection()) {

            PreparedStatement ps = conn.prepareStatement("insert into reimbursement values (default, ?, ?, ?, ?, ?)");

            ps.setString(1, reimbursement.getDescription());
            ps.setObject(2, reimbursement.getAmount());
            ps.setObject(3, reimbursement.getDate());
            ps.setInt(4, reimbursement.getStatus().ordinal());
            ps.setInt(5, reimbursement.getEmployee_id());

            // execute prepared statement -> number of rows affected
            int numOfRows = ps.executeUpdate();

            if(numOfRows == 1){
                return true;
            }

        } catch (SQLException e) {
            e.printStackTrace();
        }

        return false;

    }

    @Override
    public List<Reimbursement> getPendingReimbursements() {
        // 1. Establish a connection with try/catch block
        try(Connection conn = ConnectionUtil.getConnection(); Statement s = conn.createStatement();){
            // 3. Get a result from DB
            ResultSet rs = s.executeQuery("select * from reimbursement");
            // 4. Create Arraylist objects to add each row in a list
            List<Reimbursement> reimbursements = new ArrayList<>();

            while(rs.next()){
                int id = rs.getInt("reimburse_id");
                String description = rs.getString("description");
                double total = rs.getDouble("total");
                LocalDate date = rs.getObject("purchase_date", LocalDate.class);

                // Access status from the db
                int statusOrdinal = rs.getInt("status");
                // Then use that number to determine the corresponding enum value
                ReimburseStatus[] statuses = ReimburseStatus.values();
                ReimburseStatus status = statuses[statusOrdinal];

                int employee_id = rs.getInt("employee_id");

                Reimbursement r = new Reimbursement(id, description, total, date, status, employee_id);
                if(r.getStatus().equals(ReimburseStatus.PENDING)){
                    reimbursements.add(r);
                }
            }
            System.out.println(reimbursements);
            return reimbursements;


        } catch (SQLException e) {
            e.printStackTrace();
        }


        return null;

    }

    @Override
    public List<Reimbursement> getApprovedReimbursements() {
        // 1. Establish a connection with try/catch block
        try(Connection conn = ConnectionUtil.getConnection(); Statement s = conn.createStatement();){
            // 3. Get a result from DB
            ResultSet rs = s.executeQuery("select * from reimbursement");
            // 4. Create Arraylist objects to add each row in a list
            List<Reimbursement> reimbursements = new ArrayList<>();

            while(rs.next()){
                int id = rs.getInt("reimburse_id");
                String description = rs.getString("description");
                double total = rs.getDouble("total");
                LocalDate date = rs.getObject("purchase_date", LocalDate.class);

                // Access status from the db
                int statusOrdinal = rs.getInt("status");
                // Then use that number to determine the corresponding enum value
                ReimburseStatus[] statuses = ReimburseStatus.values();
                ReimburseStatus status = statuses[statusOrdinal];

                int employee_id = rs.getInt("employee_id");

                Reimbursement r = new Reimbursement(id, description, total, date, status, employee_id);
                if(r.getStatus().equals(ReimburseStatus.APPROVED)){
                    reimbursements.add(r);
                }
            }
            System.out.println(reimbursements);
            return reimbursements;


        } catch (SQLException e) {
            e.printStackTrace();
        }

        return null;

    }

    @Override
    public List<Reimbursement> getDeniedReimbursements() {

        // 1. Establish a connection with try/catch block
        try(Connection conn = ConnectionUtil.getConnection(); Statement s = conn.createStatement();){
            // 3. Get a result from DB
            ResultSet rs = s.executeQuery("select * from reimbursement");
            // 4. Create Arraylist objects to add each row in a list
            List<Reimbursement> reimbursements = new ArrayList<>();

            while(rs.next()){
                int id = rs.getInt("reimburse_id");
                String description = rs.getString("description");
                double total = rs.getDouble("total");
                LocalDate date = rs.getObject("purchase_date", LocalDate.class);

                // Access status from the db
                int statusOrdinal = rs.getInt("status");
                // Then use that number to determine the corresponding enum value
                ReimburseStatus[] statuses = ReimburseStatus.values();
                ReimburseStatus status = statuses[statusOrdinal];

                int employee_id = rs.getInt("employee_id");

                Reimbursement r = new Reimbursement(id, description, total, date, status, employee_id);
                if(r.getStatus().equals(ReimburseStatus.DENIED)){
                    reimbursements.add(r);
                }
            }
            System.out.println(reimbursements);
            return reimbursements;


        } catch (SQLException e) {
            e.printStackTrace();
        }

        return null;

    }

    @Override
    public List<Reimbursement> getReimbursementsByEmployee(int employeeId) {
        // 1. Establish a connection with try/catch block
        try(Connection conn = ConnectionUtil.getConnection(); Statement s = conn.createStatement();){
            // 3. Get a result from DB
            ResultSet rs = s.executeQuery("select * from reimbursement");
            // 4. Create Arraylist objects to add each row in a list
            List<Reimbursement> reimbursements = new ArrayList<>();

            while(rs.next()){
                int id = rs.getInt("reimburse_id");
                String description = rs.getString("description");
                double total = rs.getDouble("total");
                LocalDate date = rs.getObject("purchase_date", LocalDate.class);

                // Access status from the db
                int statusOrdinal = rs.getInt("status");
                // Then use that number to determine the corresponding enum value
                ReimburseStatus[] statuses = ReimburseStatus.values();
                ReimburseStatus status = statuses[statusOrdinal];
                int employee_id = rs.getInt("employee_id");

                Reimbursement r = new Reimbursement(id, description, total, date, status, employee_id);
                if(r.getEmployee_id() == employeeId){
                    reimbursements.add(r);
                }
            }
            //System.out.println(reimbursements);
            return reimbursements;


        } catch (SQLException e) {
            e.printStackTrace();
        }

        return null;

    }

    @Override
    public boolean updateReimbursement(Reimbursement reimbursement) {

        //validate input to make sure it's not null
        if(reimbursement == null){
            return false;
        }
        //validate input to make sure it has all of the important fields
        if(reimbursement.getDescription() == null || reimbursement.getAmount() == 0 || reimbursement.getDate() == null){
            return false;
        }

        try(Connection conn = ConnectionUtil.getConnection()) {

            PreparedStatement ps = conn.prepareStatement("update reimbursement set ? where reimburse_id = ?");

            ps.setString(1, reimbursement.getDescription());
            ps.setObject(2, reimbursement.getAmount());
            ps.setObject(3, reimbursement.getDate());
            ps.setInt(4, reimbursement.getStatus().ordinal());
            ps.setInt(5, reimbursement.getEmployee_id());

            // execute prepared statement -> number of rows affected
            int numOfRows = ps.executeUpdate();

            if(numOfRows == 1){
                return true;
            }

        } catch (SQLException e) {
            e.printStackTrace();
        }


        return false;

    }
}
