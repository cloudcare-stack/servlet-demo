package com.revature.models;


import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.datatype.jsr310.deser.LocalDateDeserializer;
import com.fasterxml.jackson.datatype.jsr310.ser.LocalDateSerializer;

import java.time.LocalDate;
import java.util.Objects;

public class Reimbursement {

    // TODO: Define Reimbursement Model
    private int id;
    private String description;
    private double amount;

    @JsonSerialize(using = LocalDateSerializer.class)
    @JsonDeserialize(using = LocalDateDeserializer.class)
    private LocalDate date;
    private ReimburseStatus status;
    private int employee_id;

    public Reimbursement() {
        super();
    }

    public Reimbursement(int id, String description, double amount, LocalDate date, ReimburseStatus status, int employee_id) {
        this.id = id;
        this.description = description;
        this.amount = amount;
        this.date = date;
        this.status = status;
        this.employee_id = employee_id;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public double getAmount() {
        return amount;
    }

    public void setAmount(double amount) {
        this.amount = amount;
    }

    public LocalDate getDate() {
        return date;
    }

    public void setDate(LocalDate date) {
        this.date = date;
    }

    public ReimburseStatus getStatus() {
        return status;
    }

    public void setStatus(ReimburseStatus status) {
        this.status = status;
    }

    public int getEmployee_id() {
        return employee_id;
    }

    public void setEmployee_id(int employee_id) {
        this.employee_id = employee_id;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Reimbursement)) return false;
        Reimbursement that = (Reimbursement) o;
        return getId() == that.getId() && Double.compare(that.getAmount(), getAmount()) == 0 && getEmployee_id() == that.getEmployee_id() && Objects.equals(getDescription(), that.getDescription()) && Objects.equals(getDate(), that.getDate()) && getStatus() == that.getStatus();
    }

    @Override
    public int hashCode() {
        return Objects.hash(getId(), getDescription(), getAmount(), getDate(), getStatus(), getEmployee_id());
    }

    @Override
    public String toString() {
        return "Reimbursement{" +
                "id=" + id +
                ", description='" + description + '\'' +
                ", amount=" + amount +
                ", date=" + date +
                ", status=" + status +
                ", employeeId=" + employee_id +
                '}';
    }
}
